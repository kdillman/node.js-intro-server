// 'requiring' modules of the node.js framework to extend functionality
var socket = require('socket.io');
var express = require('express');
var http = require('http');
var app = express();
var querystring = require("querystring");
var fs = require("fs");
var formidable = require("formidable"); 

// **************************************************************************************************** NODE SERVER SETUP
// create a server variable
var server = http.createServer(app);
// tell the server to listen on port 8888, 'log out' message
server.listen(8888);
console.log("Server started. Listening on port: 8888");
// tell the socket.io variable to listen on the created server
var io = socket.listen(server);

// use the resources folder for storage and retrieval of files, such as css and JQuery
app.use(express.static(__dirname + '/resources'));
// remove unneeded 'junk' output from console messages
io.set("log level", 1);

// listen for the get request for "/" or "localhost:8888" from the browser
app.get("/", function(request, response){	
	// respond with the index file for the server
	response.sendfile(__dirname+"/index.html");
});

// listen for the get request for "/chat" from the browser
app.get("/chat", function(request, response){	
	// respond with the index file for the chat application
	response.sendfile(__dirname+"/chat.html");
});

// listen for the get request for "/file" from the browser
app.get("/file", function(request, response){	
	// set the response to render the index template
	response.render('index.ejs', {
		layout:false
	});
});


// **************************************************************************************************** CHAT APPLICATION
// on a 'connection' event from the socket.io variable, run the internal code
io.sockets.on('connection', function(client){
	// 'log out' a connection message to the server output
	console.log('Client connected.');
	// listen for 'messages' events, run internal code if heard
	client.on('messages', function(data){
		// 'log out' incoming messages to the server output
		console.log(data);
		// get the client nickname before broadcasting message
		client.get('nickname', function(err, name){
			// broadcast message with the given nickname to all connected clients using the 'emit' command
			client.broadcast.emit("messages", name + ": " + data);
		});	
	});
	
	// listen for 'join' events, run internal code if heard
	client.on('join', function(name){
		// set the nickname with joined client heard from the event
		client.set('nickname', name);
		// broadcast a message that the client has connected to the chat
		client.broadcast.emit("messages", name + " has connected.");
	});
});

// **************************************************************************************************** FILE APPLICATION
// the server hears the POST data from a form sending an "/upload" event
app.post("/upload", function(request, response) {
	// set formidable variable to parse and manipulate the form data sent
	var form = formidable.IncomingForm();
	// allow multiple files to be parsed and used with the formidable object
	form.multiples = true;
	// parse the request, which prepares the data for usage with the application
	form.parse(request);
	// set the formidable object to listen to any "file" events, and run the inter function when they are heard
	form.on('file', function(name, file) {
		// set a variable to equal the filename for manipulation
		var newname = file.name;
		// set an array to hold the values of the filename by splitting the name by the file extension (example.png)
		var nameArray = newname.split(".");
		// set the new name of the file by adding a random generated number to the middle of the name and adding the file extension back
		newname = nameArray[0] + (Math.random()*(100-1)+1) + "." + nameArray[1];
		// "rename" or upload the file to the "/images" folder on the server, run the internal function if there is an error
		fs.rename(file.path, __dirname+"/resources/images/"+newname, function(err) {
				if (err) { // this is run if there is a duplicate file in the folder
					// if there is an error, 'log out' the written error and message
					console.log("upload error has occured: " + err); 
					// ""unlink" or delete the original file found
					fs.unlink(newname); 
					// add the queued file to the folder in the place of the previous file
					fs.rename(file.path, __dirname+"/resources/images/"+newname);
				} 
		});
	});
	// end function is called when the form upload is complete
	form.on('end', function(){
		// set the response to render the upload template to direct the user to the "/show" page
		response.render('upload.ejs', {
			layout:false
		});	
	});
});

// listen for the get request for "/show" from the browser
app.get("/show", function(request, response) { 
	// read the directory of the "/images" folder and ru the internal function
	fs.readdir(__dirname+"/resources/images", function (err, files) { 
		// if an error occurs, run the internal function
		if(err) { 
			// set the response to render the error template
			response.render('error.ejs', {
				layout:false
			});
		}
		else{
			// create an array to hold the names of the images in the folder
			var images = new Array(); 
			// loop through each file in the directory
			files.forEach( function (file) {
				// 'log out' a message
				console.log("reading '/images' contents");
				// as each the item is read, the name of the file is "pushed" or added to the array
				images.push("images/"+file);
			});
			// set the response to render the show template
			response.render('show.ejs', {
				// when the page renders, add "data" to be accessed by the page, which is the image name array
				data: images
			});		
		}
	}); 
});