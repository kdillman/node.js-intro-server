Node.JS Introduction & Tutorial Server
	Written/Coded by Keegan Dillman
	
Welcome to my educational example of  the functionality and power of the node.js framework!
This text file will guide you through getting your computer ready to host the tutorial server, you
will need:
	- A Command (cmd) window: to run the intial start-up commands.
	- A text editor of choice: to view the code to aid in learning it.
	- A browser (any kind): to view the contents and navigate the tutorial.
	
These steps will get you started and up and running with the server:
(It is assumed you are running a Windows machine, for the installation on other OS types and
 releases, look in "howtonode.com" for installation steps.)

	1) In a browser, navigate to "nodejs.org/download" and choose the appropriate Windows Installer.
	2) Run the installer and reset the computer when needed.
	3) In your command window, navigate to the node tutorial "Project" file. 
		(example: "C:\Users\Keegan\Desktop\Project")
	4) Run the following commands: 
		"npm install socket.io"
		"npm install express"
		"npm install formidable"
		"npm install querystring"
		"npm install ejs"
		These will install the needed node modules to run the application server and it's examples.
	5) Run the following command:
		"node app.js"
		This will start the server, and it will listen on port 8888 on your computer for interaction, keep
		this window open to view the logged out messages in the application's runtime.
	6) In a browser, navigate to "localhost:8888"
		This will bring to the main page of the tutorial application.
		
All set! You can read and navigate the tutorial from here!